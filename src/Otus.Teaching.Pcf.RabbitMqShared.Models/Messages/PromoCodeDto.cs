﻿using System;

namespace Otus.Teaching.Pcf.RabbitMqShared.Models.Messages
{
    public class PromoCodeDto
    {
        public Guid Id { get; set; }
        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public Guid PartnerId { get; set; }
    }
}
