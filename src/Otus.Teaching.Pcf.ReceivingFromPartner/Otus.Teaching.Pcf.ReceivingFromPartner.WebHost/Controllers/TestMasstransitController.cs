﻿using Bogus;
using CommonNamespace;
using MassTransit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Services;
using System;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestMasstransitController : ControllerBase
    {
        private readonly MasstransitService<MessageDto> _masstransit;


        public TestMasstransitController(MasstransitService<MessageDto> masstransit, IBus bus)
        {
            _masstransit=masstransit;

        }
        [HttpPost("publish")]

        public async Task<IActionResult> PublishMessageAsync()
        {
            var test = new MessageDto { Content = Guid.NewGuid().ToString() };

            await _masstransit.PublishMessageAsync(test);


            return Ok(test);

        }
        [HttpPost("send")]
        public async Task<IActionResult> SendMessageAsync()
        {
            var test = new MessageDto { Content = "Message!!" };

            await _masstransit.SendMessageAsync(test);


            return Ok(test);

        }
    }
}
