﻿using System;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using CommonNamespace;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Services;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class AdministrationGateway
        : IAdministrationGateway
    {
        private readonly HttpClient _httpClient;
        private readonly MasstransitService<MessageDto> _masstransit;
        public AdministrationGateway(HttpClient httpClient, MasstransitService<MessageDto> masstransit)
        {
            _httpClient = httpClient;
            _masstransit=masstransit;
        }

        public async Task NotifyAdminAboutPartnerManagerPromoCode(Guid partnerManagerId)
        {
            await _masstransit.PublishMessageAsync(new MessageDto { Content = JsonSerializer.Serialize(partnerManagerId) });

        }
    }
}