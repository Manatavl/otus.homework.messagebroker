﻿using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using CommonNamespace;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Services;
using Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration
{
    public class GivingPromoCodeToCustomerGateway
        : IGivingPromoCodeToCustomerGateway
    {
        private readonly HttpClient _httpClient;
        private readonly MasstransitService<MessageDto> _masstransit;
        public GivingPromoCodeToCustomerGateway(HttpClient httpClient, MasstransitService<MessageDto> masstransit)
        {
            _httpClient = httpClient;
            _masstransit=masstransit;
        }

        public async Task GivePromoCodeToCustomer(PromoCode promoCode)
        {
            var dto = new GivePromoCodeToCustomerDto()
            {
                PartnerId = promoCode.Partner.Id,
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                EndDate = promoCode.EndDate.ToShortDateString(),
                PreferenceId = promoCode.PreferenceId,
                PromoCode = promoCode.Code,
                ServiceInfo = promoCode.ServiceInfo,
                PartnerManagerId = promoCode.PartnerManagerId
            };
            var message = new MessageDto() { Content = JsonSerializer.Serialize(dto) };
            await _masstransit.SendMessageAsync(message);

        }
    }
}