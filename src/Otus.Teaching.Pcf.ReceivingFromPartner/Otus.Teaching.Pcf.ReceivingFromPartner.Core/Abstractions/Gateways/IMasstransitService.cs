﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways
{
    public interface IMasstransitService<T>
    {
        Task PublishMessageAsync(T message);
        Task SendMessageAsync(T message);
    }
}
