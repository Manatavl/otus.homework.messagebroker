﻿
using MassTransit;
using MassTransit.Transports;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain;
using System;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Services
{
    public class MasstransitService<T> : IMasstransitService<T> where T : class
    {
        private readonly IPublishEndpoint _publishEndpoint;
        private readonly ISendEndpointProvider _sendEndpoint;

        public MasstransitService(IPublishEndpoint publishEndpoint,
            ISendEndpointProvider sendEndpoint)
        {
            _publishEndpoint=publishEndpoint;
            _sendEndpoint=sendEndpoint;
        }

        public async Task PublishMessageAsync(T message)
        {
            await _publishEndpoint.Publish<T>(message);
        }

        public async Task SendMessageAsync(T message)
        {
            var queueName = "receiving-from-partner";
            var endpoint = await _sendEndpoint.GetSendEndpoint(new Uri($"queue:{queueName}"));


            // var sendEndpoint = await busControl.GetSendEndpoint(new Uri($"queue:{queueName}"));
            if (endpoint == null)
            {
                throw new Exception($"Не удалось найти очередь {queueName}");
            }
            await endpoint.Send<T>(message, CancellationToken.None);
        }
    }
}
