﻿using CommonNamespace;
using MassTransit;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class EventConsumer : IConsumer<MessageDto>
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;
        public EventConsumer(IRepository<PromoCode> promoCodesRepository,
            IRepository<Preference> preferencesRepository,
            IRepository<Customer> customersRepository)
        {
            _promoCodesRepository=promoCodesRepository;
            _preferencesRepository=preferencesRepository;
            _customersRepository=customersRepository;
        }



        public async Task Consume(ConsumeContext<MessageDto> context)
        {
            if (context.Message.Content != null)
            {
                var response = JsonSerializer.Deserialize<GivePromoCodeRequest>(context.Message.Content);
                var preference = await _preferencesRepository.GetByIdAsync(response.PreferenceId);

                //  Получаем клиентов с этим предпочтением:
                var customers = await _customersRepository
                    .GetWhere(d => d.Preferences.Any(x =>
                        x.Preference.Id == preference.Id));

                PromoCode promoCode = PromoCodeMapper.MapFromModel(response, preference, customers);

                await _promoCodesRepository.AddAsync(promoCode);

            }
        }
    }
}
