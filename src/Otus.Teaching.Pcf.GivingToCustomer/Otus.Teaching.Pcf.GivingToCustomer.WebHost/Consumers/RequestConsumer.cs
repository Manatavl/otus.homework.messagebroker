﻿using CommonNamespace;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Response = CommonNamespace.Response;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class RequestConsumer : IConsumer<Request>
    {
        public async Task Consume(ConsumeContext<Request> context)
        {
            Console.WriteLine("Value: {0}", context.Message.Message.Content);
            await context.RespondAsync<Response>(new CommonNamespace.Response
            {
                IsSuccess = true
            });
        }
    }
}
